import Model.*;
import RyuManagers.SwitchManager;
import RyuManagers.SwitchManagerInterface;
import RyuRestClient.RestClient;
import Utils.Configuration;

import java.io.IOException;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        Configuration configuration = new Configuration();

        RestClient client = new RestClient(configuration.getConfiguration());
        SwitchManagerInterface switchManager = new SwitchManager(client);

        /*
         * In this part, we creates 3 queues, then we add them into SwitchQueue and finally we add SwitchQueue to
         * first switch in topology.
         */

        try {
            Queue queue1 = new Queue(500, 100);
            Queue queue2 = new Queue(800, 80);
            Queue queue3 = new Queue(50000, 150);

            SwitchQueue switchQueue = new SwitchQueue(800, 95);
            switchQueue.addQueue(queue1).addQueue(queue2).addQueue(queue3);

            ArrayList<Switch> switches = switchManager.getSwitches();
            switchManager.setSwitchQueueToSwitch(switches.get(0), switchQueue);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        /*
         * At this point, we creates 1 MatchRule, 1 SwitchMatchQueueAction and add them to switch
         */

        try {
            // same like in first example
            Queue queue1 = new Queue(1000, 500);
            SwitchQueue switchQueue = new SwitchQueue();
            switchQueue.addQueue(queue1);
            switchManager.setSwitchQueueToSwitch(switchManager.getSwitches().get(0), switchQueue);

            // 1. create rule, 2. create action(points to queue), 3. create QoS (rule + action), 1. add to switch
            MatchRule matchRule = new MatchRule();
            matchRule.setSrcIp("10.10.10.1/24");
            matchRule.setProto(MatchRule.NetworkProto.UDP);
            matchRule.setSrcPort(5001);

            MatchAction matchAction = new MatchAction();
            matchAction.setToQueue(queue1);

            SwitchQoS switchQoS = new SwitchQoS(matchAction, matchRule);
            switchManager.addSwitchQosToSwitch(switchManager.getSwitches().get(0), switchQoS);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        /*
          This part shows how to add queues and qos to multiple switches at once
         */

        try {
            // create queues
            Queue queue1 = new Queue(500, 100);

            SwitchQueue switchQueue = new SwitchQueue(800, 95);
            switchQueue.addQueue(queue1);

            // create switch path from first two switches
            ArrayList<Switch> switches = switchManager.getSwitches();
            SwitchPath switchPath = new SwitchPath();
            switchPath.addSwitch(switches.get(0)).addSwitch(switches.get(1));

            // 1. create rule, 2. create action(points to queue), 3. create QoS (rule + action), 1. add to switch
            MatchRule matchRule = new MatchRule();
            matchRule.setSrcIp("10.10.10.1/24");
            matchRule.setProto(MatchRule.NetworkProto.UDP);
            matchRule.setSrcPort(5001);

            MatchAction matchAction = new MatchAction();
            matchAction.setToQueue(queue1);

            SwitchQoS switchQoS = new SwitchQoS(matchAction, matchRule);

            // add to switch path
            switchManager.setSwitchQosAndSwitchQueueToSwitchPath(switchPath, switchQueue, switchQoS);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
