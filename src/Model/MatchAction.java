package Model;

import org.json.JSONException;
import org.json.JSONObject;

public class MatchAction {
    private Queue toQueue;

    public void setToQueue(Queue queue) {
        this.toQueue = queue;
    }

    public Queue getToQueue() {
        return this.toQueue;
    }

    public JSONObject getActionAsJson() throws JSONException {
        JSONObject action = new JSONObject();
        action.put("queue", toQueue.getId());
        return action;
    }
}
