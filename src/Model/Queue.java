package Model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Simple Queue object, which is part of SwitchQueue
 */
public class Queue {
    private Integer maxRate;
    private Integer minRate;
    private Integer id;

    public Queue(Integer maxRate, Integer minRate) throws IOException {
        this.setMaxRate(maxRate);
        this.setMinRate(minRate);
    }

    public void setMaxRate(Integer maxRate) throws IOException {
        if (this.minRate == null || this.minRate <= maxRate)
            this.maxRate = maxRate;
        else
            throw new IOException("maxRate in queue must be greater than minRate");
    }

    public void setMinRate(Integer minRate) throws IOException {
        if (this.maxRate == null || this.maxRate >= minRate)
            this.minRate = minRate;
        else
            throw new IOException("minRate in queue must be lower than maxRate");
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public Integer getMaxRate() {
        return this.maxRate;
    }

    public Integer getMinRate() {
        return this.minRate;
    }

    public JSONObject getQueueAsJsonObject() throws JSONException {
        JSONObject queue = new JSONObject();
        if (this.maxRate != null)
            queue.put("max_rate", this.maxRate.toString());
        if (this.minRate != null)
            queue.put("min_rate", this.minRate.toString());
        return queue;
    }
}
