package Model;

import org.json.JSONException;
import org.json.JSONObject;

public class SwitchPort {
    private String name;
    private String mac;
    private String id;

    public SwitchPort(String name, String mac) {
        this.name = name;
        this.mac = mac;
    }

    public String getName() {
        return this.name;
    }

    public String getMac() {
        return this.mac;
    }

}
