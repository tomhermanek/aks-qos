package Model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class SwitchQoS {
    private MatchAction action;
    private MatchRule rule;

    public SwitchQoS(MatchAction action, MatchRule rule) {
        this.setMatchAction(action);
        this.setMatchRule(rule);
    }

    public MatchAction getAction() {
        return action;
    }

    public void setMatchAction(MatchAction action) {
        this.action = action;
    }

    public MatchRule getRule() {
        return rule;
    }

    public void setMatchRule(MatchRule rule) {
        this.rule = rule;
    }

    public JSONObject getQoSAsJson() throws JSONException {
        JSONObject qosJson = new JSONObject();
        qosJson.put("match", this.rule.getMatchAsJson());
        qosJson.put("actions", this.action.getActionAsJson());
        return qosJson;
    }
}
