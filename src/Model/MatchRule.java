package Model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Defines match rule based on some data in L2, L3 or L4
 */
public class MatchRule {
    public enum NetworkProto {
        TCP, UDP, ICMP, ICMPv6
    }

    private String srcMac;
    private String dstMac;
    private String srcIp;
    private String dstIp;
    private NetworkProto proto;
    private Integer srcPort;
    private Integer dstPort;
    private Integer dscp;

    public String getSrcMac() {
        return srcMac;
    }

    public void setSrcMac(String srcMac) {
        this.srcMac = srcMac;
    }

    public String getDstMac() {
        return dstMac;
    }

    public void setDstMac(String dstMac) {
        this.dstMac = dstMac;
    }

    public String getSrcIp() {
        return srcIp;
    }

    public void setSrcIp(String srcIp) {
        this.srcIp = srcIp;
    }

    public String getDstIp() {
        return dstIp;
    }

    public void setDstIp(String dstIp) {
        this.dstIp = dstIp;
    }

    public NetworkProto getProto() {
        return proto;
    }

    public void setProto(NetworkProto proto) {
        this.proto = proto;
    }

    public Integer getSrcPort() {
        return srcPort;
    }

    public void setSrcPort(Integer srcPort) throws IOException {
        if (this.proto != null)
            this.srcPort = srcPort;
        else
            throw new IOException("Source port cannot be used withoud specified protocol");
    }

    public Integer getDstPort() {
        return dstPort;
    }

    public void setDstPort(Integer dstPort) throws IOException {
        if (this.proto != null)
            this.dstPort = dstPort;
        else
            throw new IOException("Destination port cannot be used withoud specified protocol");
    }

    public Integer getDscp() {
        return dscp;
    }

    public void setDscp(Integer dscp) {
        this.dscp = dscp;
    }

    public JSONObject getMatchAsJson() throws JSONException {
        JSONObject matchJson = new JSONObject();
        if (this.srcMac != null)
            matchJson.put("dl_src", this.srcMac);
        if (this.dstMac != null)
            matchJson.put("dl_dst", this.dstMac);
        if (this.srcIp != null)
            matchJson.put("nw_src", this.srcIp);
        if (this.dstIp != null)
            matchJson.put("nw_dst", this.dstIp);
        if (this.proto != null)
            matchJson.put("nw_proto", this.proto.toString());
        if (this.srcPort != null)
            matchJson.put("tp_src", this.srcPort.toString());
        if (this.dstPort != null)
            matchJson.put("tp_dst", this.dstPort.toString());
        if (this.dscp != null)
            matchJson.put("ip_dscp", this.dscp.toString());
        return matchJson;
    }
}
