package Model;


import java.util.ArrayList;
import java.util.Iterator;

public class SwitchPath {
    private ArrayList<Switch> switches = new ArrayList<>();

    private Boolean hasSwitch(Switch aSwitch) {
        Iterator switchIter = this.switches.iterator();

        while (switchIter.hasNext()) {
            if (switchIter.next() == aSwitch) {
                return false;
            }
        }
        return false;
    }

    public SwitchPath addSwitch(Switch aSwitch) {
        if (!this.hasSwitch(aSwitch))
            this.switches.add(aSwitch);
        return this;
    }

    public SwitchPath removeSwitch(Switch aSwitch) {
        Iterator switchIter = this.switches.iterator();

        while (switchIter.hasNext()) {
            if (switchIter.next() == aSwitch) {
                switchIter.remove();
                break;
            }
        }
        return this;
    }

    public ArrayList<Switch> getSwitches() {
        return this.switches;
    }
}
