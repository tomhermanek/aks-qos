package Model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.StringJoiner;

public class SwitchQueue {
    private Integer maxRate;
    private Integer minRate;
    private ArrayList<Queue> queues = new ArrayList<>();

    public SwitchQueue(){};

    public SwitchQueue(Integer maxRate, Integer minRate) throws IOException {
        this.setMaxRate(maxRate);
        this.setMinRate(minRate);
    }

    public void setMaxRate(Integer maxRate) throws IOException {
        if (this.minRate == null || this.minRate <= maxRate)
            this.maxRate = maxRate;
        else
            throw new IOException("maxRate in queue must be greater than minRate");
    }

    public void setMinRate(Integer minRate) throws IOException {
        if (this.maxRate == null || this.maxRate >= minRate)
            this.minRate = minRate;
        else
            throw new IOException("minRate in queue must be lower than maxRate");
    }

    public SwitchQueue addQueue(Queue queue) {
        this.queues.add(queue);
        return this;
    }

    public ArrayList<Queue> getQueues() {
        return this.queues;
    }

    public JSONObject getSwitchQueueAsJsonObject() throws JSONException {
        JSONObject switchPortQueueJson = new JSONObject();
        if (this.maxRate != null)
            switchPortQueueJson.put("max_rate", this.maxRate.toString());
        if (this.minRate != null)
            switchPortQueueJson.put("min_rate", this.minRate.toString());

        Collection<JSONObject> queuesJson = new ArrayList<>();
        for (Queue queue:this.queues) {
            queuesJson.add(queue.getQueueAsJsonObject());
        }

        switchPortQueueJson.put("queues", queuesJson);
        return switchPortQueueJson;
    }
}
