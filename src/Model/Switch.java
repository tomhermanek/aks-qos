package Model;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class Switch {
    private String id;
    private ArrayList<SwitchPort> switchPorts = new ArrayList<>();
    public SwitchQueue queue;
    public ArrayList<SwitchQoS> qoS;      // ToDo change to List?

    public void setSwitchQueue(SwitchQueue queue) {
        this.queue = queue;
    }

    public SwitchQueue getSwitchQueue() {
        return this.queue;
    }

    public ArrayList<SwitchQoS> getSwitchQoS() {
        return this.qoS;
    }

    public Switch(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    public ArrayList<SwitchPort> getSwitchPorts() {
        return this.switchPorts;
    }

    public Switch addSwitchPort(SwitchPort switchPort) {
        this.switchPorts.add(switchPort);
        return this;
    }

    public void removeSwitchQueue(){
        this.queue = null;
    }

    public void removeSwitchQoS(SwitchQoS qoS) {
        Iterator qosIter = this.qoS.iterator();
        while (qosIter.hasNext()) {
            if (qosIter.next() == qoS ) {
                qosIter.remove();
                break;
            }
        }
    }

    public Switch addSwitchQos(SwitchQoS qoS) {
        if (this.qoS == null)
            this.qoS = new ArrayList<>();
        this.qoS.add(qoS);
        return this;
    }

    public JSONObject getSwitchQueueAsJson() throws JSONException {
        JSONObject queue = this.queue.getSwitchQueueAsJsonObject();
        queue.put("type", "linux-htb");
        return queue;
    }
}
