package RyuManagers;

import Model.*;

import java.io.IOException;
import java.util.ArrayList;

public interface SwitchManagerInterface {
    /**
     * Allows to set Queue Object to Switch. Queue is applied to each interface.
     *
     * @param aSwitch updating SwitchPort
     * @param queue SwitchQueue to add
     * @return Returns modified Switch
     */
    Switch setSwitchQueueToSwitch(Switch aSwitch, SwitchQueue queue) throws IOException;

    /**
     * Allows to add QOS rule to Switch.
     *
     * @param aSwitch Switch instance which to update
     * @param switchQoS QoS rule
     * @return Returns modified Switch
     */
    Switch addSwitchQosToSwitch(Switch aSwitch, SwitchQoS switchQoS);

    /**
     * Get array list of all Switches.
     * @return
     */
    ArrayList<Switch> getSwitches();

    /**
     * Allows to add QOS rule to multiple switches
     *
     * @param switchPath SwitchPath instance which to update
     * @param switchQueue switchQueue instance
     * @param switchQoS QoS rule
     * @return Returns modified SwitchPath
     */
    SwitchPath setSwitchQosAndSwitchQueueToSwitchPath(SwitchPath switchPath, SwitchQueue switchQueue, SwitchQoS switchQoS) throws IOException;
}
