package RyuManagers;

import Model.*;
import RyuRestClient.RestClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Manages information about switch
 */
public class SwitchManager implements SwitchManagerInterface {
    private ArrayList<Switch> switches;
    private RestClient restClient;

    /**
     * Automatically initializes topology data
     *
     * @param restClient client instance for REST API
     */
    public SwitchManager(RestClient restClient) {
        this.restClient = restClient;
        this.switches = new ArrayList<>();
        this.initSwitches();
    }

    private void initOvsdb(Switch aSwitch) throws JSONException {
        //this.restClient.put("/v1.0/conf/switches/"+aSwitch.getId()+"/ovsdb_addr", "\"tcp:127.0.0.1:6632\"");
    }

    /**
     * Initializes switch array using Rest Ryu
     */
    private void initSwitches() {
        JSONArray jsonArray = this.restClient.get("/v1.0/topology/switches", "");

        for (int i = 0 ; i < jsonArray.length(); i++) {
            try {
                JSONObject switchItem = jsonArray.getJSONObject(i);
                String switchId = switchItem.getString("dpid");
                Switch aswitch = new Switch(switchId);

                JSONArray portsArray = switchItem.getJSONArray("ports");

                for (int j = 0; j < portsArray.length(); j++) {
                    JSONObject portItem = portsArray.getJSONObject(j);
                    String portName = portItem.getString("name");
                    String macAddr = portItem.getString("hw_addr");
                    SwitchPort switchPort = new SwitchPort(portName, macAddr);

                    aswitch.addSwitchPort(switchPort);
                    System.out.println("Added port: "+switchPort.getName()+" to switch id: "+aswitch.getId());
                }

                this.initOvsdb(aswitch);
                this.switches.add(aswitch);
                System.out.println("Added switch id: "+aswitch.getId());

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        jsonArray.length();
    }

    public Switch setSwitchQueueToSwitch(Switch aSwitch, SwitchQueue switchQueue) throws IOException {
        // add queue to forwarder using API, if success then add to SwitchPort instance
        aSwitch.setSwitchQueue(switchQueue);
        try {
            JSONObject switchPortJsonQueue = aSwitch.getSwitchQueueAsJson();
            JSONArray jsonResult = this.restClient.post("/qos/queue/"+aSwitch.getId(), switchPortJsonQueue);
            System.out.println("Adding switch queue to switch id: "+aSwitch.getId());

            if (jsonResult != null) {
                ArrayList<Queue> switchQueues = aSwitch.getSwitchQueue().getQueues();
                for (int i = 0 ; i < jsonResult.length(); i++) {
                    JSONObject switchResult = jsonResult.getJSONObject(i).getJSONObject("command_result").getJSONObject("details");
                    Iterator iterator = switchResult.keys();

                    while (iterator.hasNext()) {
                        String key = (String) iterator.next();
                        JSONObject resultQueueConfig = switchResult.getJSONObject(key).getJSONObject("config");
                        //verify queue
                        Queue aktQueue = switchQueues.get(Integer.parseInt(key));

                        if (aktQueue.getMaxRate() == resultQueueConfig.getInt("max-rate") && aktQueue.getMinRate() == resultQueueConfig.getInt("min-rate")) {
                            aktQueue.setId(Integer.parseInt(key));
                            System.out.println("Added queue id: "+aktQueue.getId()+" with parameters: "+aktQueue.getMinRate()+"-"+aktQueue.getMaxRate());
                        }
                        else
                            throw new IOException("Queue id"+ key +" incompatibility with mininet queue");
                    }
                }
            }
            else {
                System.out.print("Failed to add Switch Queue to swich: "+aSwitch.getId());
                aSwitch.removeSwitchQueue();
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }

        return aSwitch;
    }

    public Switch addSwitchQosToSwitch(Switch aSwitch, SwitchQoS switchQoS) {
        aSwitch.addSwitchQos(switchQoS);
        try {
            JSONObject switchQosJson = switchQoS.getQoSAsJson();
            JSONArray jsonArray = this.restClient.post("/qos/rules/"+aSwitch.getId(), switchQosJson);
            if (jsonArray != null) {
                System.out.println("Added switch QoS to switch id: "+aSwitch.getId());

            }
            else {
                aSwitch.removeSwitchQoS(switchQoS);
                System.out.println("Failed to add QoS rule to switch");
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return aSwitch;
    }

    public SwitchPath setSwitchQosAndSwitchQueueToSwitchPath(SwitchPath switchPath, SwitchQueue switchQueue, SwitchQoS switchQoS) throws IOException {
        ArrayList<Switch> switches = switchPath.getSwitches();
        Iterator switchIterator = switches.iterator();

        while (switchIterator.hasNext()) {
            Switch actualSwitch = (Switch) switchIterator.next();
            this.setSwitchQueueToSwitch(actualSwitch, switchQueue);
            this.addSwitchQosToSwitch(actualSwitch, switchQoS);
        }

        return switchPath;
    }

    public ArrayList<Switch> getSwitches() {
        return this.switches;
    }
}
