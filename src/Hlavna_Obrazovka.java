import Model.*;
import RyuManagers.SwitchManager;
import RyuManagers.SwitchManagerInterface;
import RyuRestClient.RestClient;
import Utils.Configuration;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by peterpaska on 9.12.16.
 */
public class Hlavna_Obrazovka {
    private JPanel hlavny_panel;
    private JButton button1;
    public JTextField min;
    public JTextField max;
    private JButton addToSwitchButton;
    private JButton okButton;
    public JComboBox switchesCombobox;
    private JComboBox queueCombobox;
    private JButton delQueueButton;
    private JButton resetButton;
    private JButton addButton;
    private JTextField MACsrc;
    private JTextField MACdst;
    private JTextField IPsrc;
    private JTextField IPdst;
    private JTextField PortSrc;
    private JTextField PortDst;
    private JTextField Dscp;
    private JButton resetButton1;
    private JComboBox queueComboboxQoS;
    private JComboBox switchesComboboxQoS;
    private JComboBox ProtoComboBox;
    private JButton addToPathButton;
    private JButton switchPathButton;
    private JComboBox sw1Combobox;
    private JComboBox sw2Combobox;
    private JComboBox switchCombobox;
    private JTextArea vypis;
    List<Queue> queues = new ArrayList<Queue>();
    ArrayList<Switch> switches;
    SwitchManagerInterface switchManager;
    public SwitchQueue switchQueue;
    public SwitchQoS switchQoS;
    public SwitchPath switchPath;

    public Hlavna_Obrazovka() {
        init();
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Queue  q;
                q=VytvorQueue(Integer.parseInt(max.getText()),Integer.parseInt(min.getText()));
                naplnQueueCombobox();
                naplnQueueComboboxQoS();
            }
        });
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switchQueue();
                switches = switchManager.getSwitches();
                naplnSwtichesCombobox();
                naplnSwtichesComboboxQoS();
                naplnSw1ComboboxQoS();
                naplnSw2ComboboxQoS();
                naplnSwitchCombobox();
            }
        });
        addToSwitchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    switchManager.setSwitchQueueToSwitch(switches.get(switchesCombobox.getSelectedIndex()), switchQueue);
                }catch (IOException er) {
                    er.printStackTrace();
                }
            }
        });
        delQueueButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                queues.remove(queueCombobox.getSelectedIndex());
                naplnQueueCombobox();
                naplnQueueComboboxQoS();
            }
        });
        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                min=max=null;
                queues.clear();
                naplnQueueCombobox();
                naplnSwtichesCombobox();
                naplnQueueComboboxQoS();
                naplnSwtichesComboboxQoS();
            }
        });
        resetButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IPsrc=IPdst=MACdst=MACsrc=PortDst=PortSrc=Dscp=null;
                ProtoComboBox.setSelectedIndex(0);
                naplnQueueCombobox();
                naplnSwtichesCombobox();
                naplnQueueComboboxQoS();
                naplnSwtichesComboboxQoS();
            }
        });
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    MatchRule matchRule = new MatchRule();
                    if((!IPsrc.getText().isEmpty())){
                        matchRule.setSrcIp(IPsrc.getText());
                    }
                    if((!IPdst.getText().isEmpty())){
                        matchRule.setDstIp(IPdst.getText());
                    }
                    if((!MACdst.getText().equals(""))){
                        matchRule.setDstMac(MACdst.getText());
                    }
                    if((!MACsrc.getText().equals(""))){
                        matchRule.setSrcMac(MACsrc.getText());
                    }
                    if((!ProtoComboBox.getSelectedItem().equals(""))){
                        matchRule.setProto((MatchRule.NetworkProto) ProtoComboBox.getSelectedItem());
                    }
                    if((!PortSrc.getText().equals(""))){
                        matchRule.setSrcPort(Integer.parseInt(PortSrc.getText()));
                    }
                    if((!PortDst.getText().equals(""))){
                        matchRule.setDstPort(Integer.parseInt(PortDst.getText()));
                    }

                    if((!Dscp.getText().equals(""))){
                        matchRule.setDscp(Integer.parseInt(Dscp.getText()));
                    }

                    MatchAction matchAction = new MatchAction();
                    matchAction.setToQueue(queues.get(queueComboboxQoS.getSelectedIndex()));

                    switchQoS = new SwitchQoS(matchAction, matchRule);
                    switchManager.addSwitchQosToSwitch(switchManager.getSwitches().get(switchesCombobox.getSelectedIndex()), switchQoS);


                }catch (IOException er) {
                    er.printStackTrace();
                }
            }
        });
        addToPathButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    switchManager.setSwitchQosAndSwitchQueueToSwitchPath(switchPath, switchQueue, switchQoS);
                }catch (IOException er) {
                    er.printStackTrace();
                }
            }
        });
        switchPathButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switchPath = new SwitchPath();
                switchPath.addSwitch(switches.get(sw1Combobox.getSelectedIndex())).addSwitch(switches.get(sw2Combobox.getSelectedIndex()));
            }
        });
        switchCombobox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String text ="";
                text = "Switch:"+ "\n";
                text= text + "ID:"+ "\n";
                text = text + (switches.get(switchCombobox.getSelectedIndex()).getId()) + "\n";

                for (int x = 0; x < switches.get(switchCombobox.getSelectedIndex()).getSwitchPorts().size(); x++) {
                    text = text + "Port " + (x+1) + "\n";
                    text = text + "Mac:" + "\n";
                    text = text + (switches.get(switchCombobox.getSelectedIndex()).getSwitchPorts().get(x).getMac()) + "\n";
                    text = text + "Name:" + "\n";
                    text = text + (switches.get(switchCombobox.getSelectedIndex()).getSwitchPorts().get(x).getName()) + "\n";
                }
                if(switches.get(switchCombobox.getSelectedIndex()).queue!=null) {
                    for(int x = 0;x<switches.get(switchCombobox.getSelectedIndex()).getSwitchQueue().getQueues().size();x++) {
                        text = text + "ID: ";
                        text = text + (switches.get(switchCombobox.getSelectedIndex()).getSwitchQueue().getQueues().get(x).getId()) + "\n";
                        text = text + "Max rate:" + "\n";
                        text = text + (switches.get(switchCombobox.getSelectedIndex()).getSwitchQueue().getQueues().get(x).getMaxRate()) + "\n";
                        text = text + "Min rate:" + "\n";
                        text = text + (switches.get(switchCombobox.getSelectedIndex()).getSwitchQueue().getQueues().get(x).getMinRate()) + "\n";
                    }
                }
                if(switches.get(switchCombobox.getSelectedIndex()).qoS!=null) {
                    text= text + "\n" + "QoS:"+ "\n";
                    for(int x = 0;x<1;x++) {
                        //for(int x = 0;x<switches.get(switchCombobox.getSelectedIndex()).getSwitchQoS().size();x++) {
                        text = text + "\n" + "Action: " + "\n";
                        text = text + "Id: " + "\n";
                        text = text + (switches.get(switchCombobox.getSelectedIndex()).getSwitchQoS().get(x).getAction().getToQueue().getId()) + "\n";
                        text = text + "Max rate: " + "\n";
                        text = text + (switches.get(switchCombobox.getSelectedIndex()).getSwitchQoS().get(x).getAction().getToQueue().getMaxRate()) + "\n";
                        text = text + "Min rate: " + "\n";
                        text = text + (switches.get(switchCombobox.getSelectedIndex()).getSwitchQoS().get(x).getAction().getToQueue().getMinRate()) + "\n";
                        text = text + "\n" + "Rule: " + "\n";
                        if(switches.get(switchCombobox.getSelectedIndex()).getSwitchQoS().get(x).getRule().getSrcIp()!=null){
                            text = text + "SrcIP: ";
                            text = text + (switches.get(switchCombobox.getSelectedIndex()).getSwitchQoS().get(x).getRule().getSrcIp()) + "\n";
                        }
                        if(switches.get(switchCombobox.getSelectedIndex()).getSwitchQoS().get(x).getRule().getDstIp()!=null) {
                            text = text + "DstIP: ";
                            text = text + (switches.get(switchCombobox.getSelectedIndex()).getSwitchQoS().get(x).getRule().getDstIp()) + "\n";
                        }
                        if(switches.get(switchCombobox.getSelectedIndex()).getSwitchQoS().get(x).getRule().getSrcMac()!=null) {
                            text = text + "SrcMac: ";
                            text = text + (switches.get(switchCombobox.getSelectedIndex()).getSwitchQoS().get(x).getRule().getSrcMac()) + "\n";
                        }
                        if(switches.get(switchCombobox.getSelectedIndex()).getSwitchQoS().get(x).getRule().getDstMac()!=null) {
                            text = text + "DstMac: ";
                            text = text + (switches.get(switchCombobox.getSelectedIndex()).getSwitchQoS().get(x).getRule().getDstMac()) + "\n";
                        }
                        if(switches.get(switchCombobox.getSelectedIndex()).getSwitchQoS().get(x).getRule().getDscp()!=null) {
                            text = text + "Dscp: ";
                            text = text + (switches.get(switchCombobox.getSelectedIndex()).getSwitchQoS().get(x).getRule().getDscp()) + "\n";
                        }
                        if(switches.get(switchCombobox.getSelectedIndex()).getSwitchQoS().get(x).getRule().getSrcPort()!=null) {
                            text = text + "SrcPort: ";
                            text = text + (switches.get(switchCombobox.getSelectedIndex()).getSwitchQoS().get(x).getRule().getSrcPort()) + "\n";
                        }
                        if(switches.get(switchCombobox.getSelectedIndex()).getSwitchQoS().get(x).getRule().getDstPort()!=null) {
                            text = text + "DstPort: ";
                            text = text + (switches.get(switchCombobox.getSelectedIndex()).getSwitchQoS().get(x).getRule().getDstPort()) + "\n";
                        }
                        if(switches.get(switchCombobox.getSelectedIndex()).getSwitchQoS().get(x).getRule().getProto()!=null) {
                            text = text + "Protocol: ";
                            text = text + (switches.get(switchCombobox.getSelectedIndex()).getSwitchQoS().get(x).getRule().getProto()) + "\n";
                        }
                    }
                }
                vypis.setText(text);
            }
        });
    }
    public Queue VytvorQueue(int min, int max){
        Queue queue = null;
        try {
            queue = new Queue(min, max);
            queues.add(queue);
        }catch (IOException e) {
            e.printStackTrace();
        }
        return queue;
    }
    public void switchQueue(){
        switchQueue = new SwitchQueue();
        for(int x = 0; x< queues.size(); x++) {
            switchQueue.addQueue(queues.get(x));
        }

    }

    public void naplnSwtichesCombobox(){
        switchesCombobox.removeAllItems();
        for(int x=0;x<switches.size();x++) {
            switchesCombobox.addItem(switches.get(x).toString());
        }
    }

    public void naplnQueueCombobox(){
        queueCombobox.removeAllItems();
        for(int x = 0; x< queues.size(); x++) {
            queueCombobox.addItem(queues.get(x));
        }
    }

    public void naplnSwtichesComboboxQoS(){
        switchesComboboxQoS.removeAllItems();
        for(int x=0;x<switches.size();x++) {
            switchesComboboxQoS.addItem(switches.get(x));
        }
    }

    public void naplnQueueComboboxQoS(){
        queueComboboxQoS.removeAllItems();
        for(int x = 0; x< queues.size(); x++) {
            queueComboboxQoS.addItem(queues.get(x));
        }
    }

    public void naplnProtoComboboxQoS(){
        ProtoComboBox.addItem("");
        ProtoComboBox.addItem(MatchRule.NetworkProto.ICMP);
        ProtoComboBox.addItem(MatchRule.NetworkProto.ICMPv6);
        ProtoComboBox.addItem(MatchRule.NetworkProto.TCP);
        ProtoComboBox.addItem(MatchRule.NetworkProto.UDP);
    }

    public void naplnSw1ComboboxQoS(){
        sw1Combobox.removeAllItems();
        for(int x=0;x<switches.size();x++) {
            sw1Combobox.addItem(switches.get(x));
        }
    }

    public void naplnSw2ComboboxQoS(){
        sw2Combobox.removeAllItems();
        for(int x=0;x<switches.size();x++) {
            sw2Combobox.addItem(switches.get(x));
        }
    }

    public void naplnSwitchCombobox(){
        switchCombobox.removeAllItems();
        for(int x=0;x<switches.size();x++) {
            switchCombobox.addItem(switches.get(x));
        }
    }

    public void init(){
        Configuration configuration = new Configuration();
        RestClient client = new RestClient(configuration.getConfiguration());
        switchManager = new SwitchManager(client);
        naplnProtoComboboxQoS();
    }
    public static void main (String[] args){
        JFrame frame = new JFrame("App");
        frame.setContentPane(new Hlavna_Obrazovka().hlavny_panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);



    }
}
